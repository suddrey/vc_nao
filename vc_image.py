"""@package vc_image
"""

import sys
import time
import numpy
from cv2 import *

class Blob():
    """!@brief This class contains functions for retrieving information on a detected image blob.
    """
    def __init__(self, contour):
        self.moment = cv.Moments(cv.fromarray(contour))
        self.bounds = boundingRect(contour)
        
    def get_bounds(self):
        """!@brief Retrieves the bounds of the blob.
        @return The bounds of the blob in the format (x, y, w, h).
        """
        return self.bounds
    
    def get_moment(self):
        """!@brief Retrieves the moment of the blob.
        @return The blob moment.
        """
        return self.moment

    def get_area(self):
        """!@brief Retrieves the area in pixels of the blob.
        @return The pixel area of the blob.
        """
        return self.moment.m00

    def get_width(self):
        """!@brief Retrieves the width in pixels of the blob.
        @return The pixel width of the blob.
        """
        return self.bounds[2]

    def get_height(self):
        """!@brief Retrieves the height in pixels of the blob.
        @return The pixel height of the blob.
        """
        return self.bounds[3]

    def get_distance(self):
        """!@brief Retrieves an estimate of the distance to the blob in metres.
        @return The estimate of the distance to the blob in metres.
        """
        return (100/(self.get_width()/9.0)) / 100

    def get_centre(self):
        """!@brief Retrieves the coordinates of the centre of the blob.
        @return The coordinates of the centre of the blob.
        """
        try:
            return [self.moment.m10 / self.moment.m00,
                    self.moment.m01 / self.moment.m00]

        except ZeroDivisionError:
            return [-1, -1]

    def __str__(self):
        return 'Area: ' + str(self.get_area()) + ' - Centre: ' + str(self.get_centre())

def compute_chromaticity(image, others):
    """!@brief Computes the chromaticity of a single channel of an RGB image.

    @param channel - The channel for which we wish to compute the chromaticity.
    @param others - The other channels that the chromaticity will be calculated with.
    @return A gray-scale image representing the chromaticity adjusted original channel.
    """
    return divide(numpy.float32(image),(numpy.float32(image) + numpy.float32(others[0]) + numpy.float32(others[1])))
    
def compute_excessColour(image, others):
    """!@brief Computes the excess Colour of a single channel of an RGB image.

    @param channel - The channel for which we wish to compute the chromaticity.
    @param others - The other channels that the excess Color will be calculated with.
    @return A gray-scale image representing the excess adjusted original channel.
    """

    return (2*numpy.float32(image) - 2*numpy.float32(others[0]) - numpy.float32(others[1]))
    	
	
def get_binary(image, threshold = 0.6):
    """!@brief Converts the supplied single-channel image to a binary image based on a threshold.

    @param image - The single-channel image used to create the binary.
    @param threshold - The threshold used to determine whether a pixel is white or black. Default is 0.6.
    @return The binary image.
    """
    binary = numpy.copy(image)
    binary[image > threshold] = 255
    binary[image < threshold] = 0

    return numpy.uint8(binary)

def get_blobs(binary):
    """!@brief Retrieves a list of Blobs based on the supplied binary image.

    @param binary - The binary image containing potential Blobs.
    @return A list containing each Blob in the image or an empty list if no Blobs are found.
    """
    blobs = []

    tmp = numpy.copy(binary)
    
    contours, hierarchy = findContours(tmp, cv.CV_RETR_EXTERNAL, cv.CV_CHAIN_APPROX_NONE)
    
    for idx in range(len(contours)):
        blobs.append(Blob(contours[idx]))

    return blobs


def mask(self, image, mask):
    """!@brief Masks an image based on a supplied binary image.

    @param image - The image which we will mask.
    @param mask - The binary mask that will be applied to the image.
    @return The masked image.
    """
    return bitwise_and(image,image,mask = mask)
