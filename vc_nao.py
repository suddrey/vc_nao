"""@package vc_nao
"""

"""@mainpage The main page stuff goes here
"""

import sys
import time
import numpy
import getpass
from naoqi import ALProxy
from naoqi import motion as ALMotion
from vc_image import *

NAO_IP = '127.0.0.1'
PORT = 9559

class Motion:
        """!@brief This class contains functions for managing the locomotion of the NAO robot

        @see https://community.aldebaran-robotics.com/doc/1-14/family/robots/links_robot.html#link-definition for a diagram on the reference frame used for locomotion.
        """
        def __init__(self):
                self.motion_proxy = ALProxy('ALMotion', NAO_IP, PORT)

        def move_to(self, x, y, theta):
                """!@brief Moves the NAO to a position relative to its present location.

                @param x - The x coordinate relative to the robot
                @param y - The y coordinate relative to the robot
                @param theta - the angular rotation applied to the robot
                """
                self.motion_proxy.post.moveTo(x, y, theta)

        def move(self, vx, vy, wz):
                """!@brief Makes the NAO move at a given velocity.

                @param vx - The velocity of the robot along the x axis
                @param vy - The velocity of the robot along the y axis
                @param wz - The angular velocity of the robot around the z axis
                """
                self.motion_proxy.move(vx, vy, wz)

        def wait(self):
                """!@brief Waits for the NAO to complete its current motion.
                """
                self.motion_proxy.waitUntilMoveIsFinished()
                
        def stop(self):
                """!@brief Stops the motion of the NAO as soon as it has achieved a stable posture
                """
                self.motion_proxy.stopMove()

        def is_moving(self):
                """!@brief Checks whether the NAO is moving

                @return True if the NAO is moving, False otherwise.
                """                
                return self.motion_proxy.moveIsActive()

        def set_arm_swing(enabled = True):
                """!@brief Enables and disables the arm motions of the NAO when it is walking.

                @param enabled - True if you wish to enable arm motions, False if you wish to disable the arm motions. If no value is supplied it will default to enabling arm motions.
                """
                self.motion_proxy.setWalkArmsEnabled(enabled)

class Effectors:
        """!@brief This class contains functions for manipulating the effectors (hands) of the NAO robot.

        @see https://community.aldebaran-robotics.com/doc/1-14/family/robots/links_robot.html#link-definition for a diagram on the reference frame used by the effectors.
        """
        def __init__(self):
                self.motion_proxy = ALProxy('ALMotion', NAO_IP, PORT)
                self.lhand = 0
                self.rhand = 0

        def move_right_hand_to(self, x, y, z, duration = 2.0):
                """!@brief Moves the right hand to a position relative to the torso of the robot.

                @param x - The x position relative to the torso of the NAO
                @param y - The y position relative to the torso of the NAO
                @param z - The z position relative to the torso of the NAO
                @param duration - The time to complete this motion. This affects the speed of the motion. The default is 2.0 seconds.
                """
                transform =  self.motion_proxy.getTransform('RArm', ALMotion.FRAME_TORSO, False)
                transform[3 ] = x
                transform[7 ] = y
                transform[11] = z
                self.rhand = self.motion_proxy.post.transformInterpolation('RArm', ALMotion.FRAME_TORSO, [transform],
                                                                      7, [duration], True)

        def move_left_hand_to(self, x, y, z, duration = 2.0):
                """!@brief Moves the left hand to a position relative to the torso of the robot.

                @param x - The x position relative to the torso of the NAO
                @param y - The y position relative to the torso of the NAO
                @param z - The z position relative to the torso of the NAO
                @param duration - The time to complete this motion. This affects the speed of the motion. The default is 2.0 seconds.
                """
                transform =  self.motion_proxy.getTransform('LArm', ALMotion.FRAME_TORSO, False)
                transform[3 ] = x
                transform[7 ] = y
                transform[11] = z

                self.lhand = self.motion_proxy.post.transformInterpolation('LArm', ALMotion.FRAME_TORSO, [transform],
                                                                      7, [duration], True)

        def move_hands_to(self, x, y, z, duration = 2.0):
                """!@brief Moves both of the hands to a position relative to the torso of the robot.

                @param x - The x position relative to the torso of the NAO
                @param y - The y position relative to the torso of the NAO
                @param z - The z position relative to the torso of the NAO
                @param duration - The time to complete this motion. This affects the speed of the motion. The default is 2.0 seconds.
                """
                l_transform =  self.motion_proxy.getTransform('LArm', ALMotion.FRAME_TORSO, False)
                r_transform =  self.motion_proxy.getTransform('RArm', ALMotion.FRAME_TORSO, False)

                l_transform[3 ] = x
                l_transform[7 ] = y
                l_transform[11] = z
                r_transform[3 ] = x
                r_transform[7 ] = y
                r_transform[11] = z

                self.rhand = self.motion_proxy.post.transformInterpolation('RArm', ALMotion.FRAME_TORSO, [r_transform],
                                                                      7, [duration], True)
                self.lhand = self.motion_proxy.post.transformInterpolation('LArm', ALMotion.FRAME_TORSO, [l_transform],
                                                                      7, [duration], True)
           
        def wait(self):
                """!@brief Waits for the effectors to complete their motion.
                """  
                self.motion_proxy.wait(self.lhand, 0)
                self.motion_proxy.wait(self.rhand, 0)

        def stop(self):
                """!@brief Stops the motion of the effectors.
                """
                self.motion_proxy.stop(self.lhand)
                self.motion_proxy.stop(self.rhand)

        def open_left_hand(self):
                """!@brief Opens the left hand.
                """
                self.motion_proxy.openHand('LHand')

        def close_left_hand(self):
                """!@brief Closes the left hand.
                """
                self.motion_proxy.closeHand('LHand')

        def open_right_hand(self):
                """!@brief Opens the right hand.
                """
                self.motion_proxy.openHand('RHand')

        def close_right_hand(self):
                """!@brief Closes the right hand.
                """
                self.motion_proxy.closeHand('RHand')

        def open_hands(self):
                """!@brief Opens both hand simultaneously.
                """
                task_id1 = self.motion_proxy.post.openHand('LHand')
                task_id2 = self.motion_proxy.post.openHand('RHand')

                self.motion_proxy.wait(task_id1, 0)
                self.motion_proxy.wait(task_id2, 0)

        def close_hands(self):  
                """!@brief Closes both hand simultaneously.
                """
                task_id1 = self.motion_proxy.post.closeHand('LHand')
                task_id2 = self.motion_proxy.post.closeHand('RHand')

                self.motion_proxy.wait(task_id1, 0)
                self.motion_proxy.wait(task_id2, 0)

class Joints:
        """!@brief This class contains functions for getting and setting various joints of the NAO robot such as its head joints.
        """
        def __init__(self):
                self.motion_proxy = ALProxy('ALMotion', NAO_IP, PORT)

        def set_angle(self, joint_names, angles, speed = 0.1):
                """!@brief Sets the angles of the supplied joints over time.

                @param joint_names - Either a specificed joint name, or else a list of joints.
                @param angles - Either a single angle, or else a list of angles for each joint.
                @param speed - The speed with which to make the change. The default is 0.1.
                """
                self.motion_proxy.angleInterpolationWithSpeed(joint_names, angles, speed)

        def set_relative_angle(self, joint_names, angles, speed = 0.1):
                """!@brief Sets the relative angle of the supplied joints over time.

                @param joint_names - Either a specificed joint name, or else a list of joints.
                @param angles - Either a single angle, or else a list of angles for each joint.
                @param speed - The speed with which to make the change. The default is 0.1.
                """
                current = self.get_angle(joint_names)

                if len(current) != len(angles):
                        print 'Must supply ' + len(current) + ' angles - Only ' + len(angles) + ' supplied.'
                        return

                for idx in range(len(angles)):
                        angles[idx] += current[idx]

                self.set_angle(joint_names, angles, speed)
                
        def get_angle(self, joint_names):
                """!@brief Retrieves the current angles of the requested joint.

                @param joint_names - Either a specificed joint name, or else a list of joint.
                @return The list of angles for the requested joint.
                """
                return self.motion_proxy.getAngles(joint_names, True)
   
        def set_stiffness(self, joint, stiffness):
                """!@brief Sets the level of stiffness of for the specified joint.
                @param joint - The joint which will be updated
                @param stiffness - The new level of stiffness for the specified joint(1 being full stiffness and 0 being no stiffness).
                """
                self.motion_proxy.stiffnessInterpolation(joint, stiffness, 1.0)
                time.sleep(1)

        def get_transform(self, name, space):
                return self.motion_proxy.getTransform(name, space, True)
                
class Posture:
        """!@brief This class contains functions for setting the posture of the NAO robot such as standing or sitting.

        @see https://community.aldebaran-robotics.com/doc/2-1/family/robots/postures_robot.html#robot-postures for a complete list of available postures.
        """
        def __init__(self):
                self.posture_proxy = ALProxy('ALRobotPosture', NAO_IP, PORT)
                self.motion_proxy = ALProxy('ALMotion', NAO_IP, PORT)
                  
        def stand(self):
                """!@brief Makes the NAO move into a standing posture.
                """
                self.motion_proxy.stiffnessInterpolation('Body', 1.0, 1.0)
                self.go_to_posture('Stand')

        def crouch(self):
                """!@brief Makes the NAO move into a crouching posture.
                """
                self.go_to_posture('Crouch')
                self.motion_proxy.stiffnessInterpolation('Body', 0.0, 1.0)

        def go_to_posture(self, posture_name):
                """!@brief Makes the NAO move into a requested posture.
                """
                self.posture_proxy.goToPosture(posture_name, 0.5)

        def get_posture_list(self):
                """!@brief Retrieves the list of available postures.

                @return The available postures on the robot
                """
                return self.posture_proxy.getPostureList()
             
class Speech:
        """!@brief This class contains functions that allow the NAO robot to speak
        """
        def __init__(self):
                self.speech_proxy = ALProxy('ALTextToSpeech', NAO_IP, PORT)

        def say(self, text):
                """!@brief Makes the NAO say the supplied text

                @param text - The text to say
                """
                self.speech_proxy.say(text)

class Vision:
        """!@brief This class contains functions that allow the NAO robot to see
        """
        TOP = 0
        BTM = 1
        
        def __init__(self):
                self.camera_proxy = ALProxy('ALVideoDevice', NAO_IP, PORT)

                # Motion proxy is needed for transforms
                self.motion_proxy = ALProxy('ALMotion', NAO_IP, PORT)

                for orphan in self.camera_proxy.getSubscribers():
                        self.camera_proxy.unsubscribe(orphan)
                        
                self.top_name = self.camera_proxy.subscribeCamera('top-camera', self.TOP, 2, 13, 15)
                self.btm_name = self.camera_proxy.subscribeCamera('btm-camera', self.BTM, 2, 13, 15)
                
        def get_image(self, camera_id = 0):
                """!@brief Gets an image from one of the NAO's two cameras

                @param camera_id - The ID of the camera (0 for top, 1 for bottom). If no ID is supplied, the top camera is used.
                @return The image retrieved from the specified camera.
                """
                selected_name = self.get_camera_name(camera_id)

                raw = self.camera_proxy.getImageRemote(selected_name)
                img = numpy.fromstring(raw[6], dtype=numpy.uint8).reshape(raw[1], raw[0], 3)
                self.camera_proxy.releaseImage(selected_name)

                return img
        
        def get_resolution(self, camera_id = 0):
                """!@brief Gets an resolution of the requested camera.

                @param camera_id - The ID of the camera (0 for top, 1 for bottom). If no ID is supplied, the resolution of the top camera is returned.
                @return The resolution of the specified camera.
                """
                resolutions = [[120.0, 160.0],
                               [320.0, 240.0],
                               [640.0, 480.0],
                               [1280.0, 960.0]]
                
                resolution_idx = self.camera_proxy.getResolution(self.get_camera_name(camera_id))

                return resolutions[resolution_idx]

        def get_field_of_view(self):
                """!@brief Gets an field of view of the cameras. This is a convenience function as the field of view is constant.

                @return The x horizontal and vertical field of view of the cameras.
                """
                return [60.9, 47.6]
                
        def get_camera_name(self, camera_id):
                if camera_id != self.TOP:
                        return self.btm_name
                
                return self.top_name

        def get_transform(self, camera_id, space):
                if camera_id != self.TOP:
                        return self.motion_proxy.getTransform('CameraBottom', space, True)
                return self.motion_proxy.getTransform('CameraTop', space, True)
        
motion = Motion()
posture = Posture()
speech = Speech()
vision = Vision()
effectors= Effectors()
joints = Joints()
